package ee.bcs.valiit;

public class Yl1 {


    public static void main(String[] args) {
        String tallinnPopulation = "450 000";

        System.out.println(String.format("Tallinnas elab %s inimest", tallinnPopulation));

        String[] numArray = tallinnPopulation.split(" ");
        StringBuffer stringBuffer = new StringBuffer();
        for (String num: numArray) {
            stringBuffer.append(num);
        }
        Integer populationOfTallinn = Integer.valueOf(stringBuffer.toString());
        System.out.println(String.format("Tallinnas elab %d inimest", populationOfTallinn));


    }
}

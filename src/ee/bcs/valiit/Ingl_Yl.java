package ee.bcs.valiit;

public class Ingl_Yl {

    public static void main(String[] args) {
        String Color = "GREEN".toLowerCase();
        if(Color.equalsIgnoreCase("green")) {
            System.out.println("Driver can drive a car");
        }
        else if(Color.equalsIgnoreCase("yellow")) {
            System.out.println("Driver has to be ready to stop the car or to start driving");
        }
        else if(Color.equalsIgnoreCase("red")) {
            System.out.println("Driver has to stop car and wait for green light");
        }
        else {
            System.out.println("Look at the traffic light!");
        }

        switch (Color) {
            case "green": System.out.println("Driver can drive a car");
            break;
            case "yellow": System.out.println("Driver has to be ready to stop the car or to start driving");
            break;
            case "red": System.out.println("Driver has to stop car and wait for green light");
            break;
            default: System.out.println("Look at the traffic light!");
            break;

        }
    }
}
